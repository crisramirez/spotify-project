
<?php
$title = isset($title) ? $title : 'Music Player by CR7';
$v = date('YmdHi');
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $title ?></title>
    <!-- bootstrap, font awesome and jquery-ui css -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">

    <!-- custom css files -->
    <link href="/spotify/assets/css/presets.css?v=<?= $v ?>" rel="stylesheet">
    <link href="/spotify/assets//css/style.css?v=<?= $v ?>" rel="stylesheet">

    <!-- bootstrap, jquery and jquery-ui js -->
    <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- custom javascript -->
    <script src="/spotify/assets/js/main.js?v=<?= $v ?>"></script>
</head>