<div class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span class="navbar-brand">
                <a href="<?= ROOT ?>">Spotify</a>
                <?php if(isset($user['name'])): ?>
                     <span class="text-small">Logged in as:<?= ucwords($user['name']) ?></i>
                <?php endif; ?>
            </span>
        </div>

        <div class="collapse navbar-collapse" id="main-menu">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?= ROOT ?>/accounts/logout.php"><i class="fa fa-sign-out"></i></a></li>
            </ul>
        </div>
    </div>
</div>

