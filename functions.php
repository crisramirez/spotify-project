<?php

/*
 * This file contains common functions that can/will be used across the application
 */

/*
 * back end errors - it will exit the application and display a blank screen with error message
 * @param $message
 */
function error($message)
{
    exit($message);
}

/**
 * display errors/warnings in front end
 * @param $message
 */
function errorMsg($message)
{
    echo '<div class="add-padding-top add-padding-bottom text-center bg-danger text-danger">' . $message . '</div>';
}

/**
 * Creates the connection to database
 * @return mysqli
 */
function dbConnect()
{
    //connect to database - using constants
    $link = mysqli_connect(DB_HOST, DB_USERNAME, DB_PW, DB_NAME);
    $error = mysqli_error($link);

    //check for errors
    if (mysqli_error($link)) {
        error($error); //custom function
    }

    return $link;
}

/**
 * queries database for user by id
 * @param $user_id
 * @return array|null
 */
function getUser($user_id)
{
    //connect to database
    $link = dbConnect(); //custom function
    $query = "SELECT `id`, `username`, `name` FROM `users` WHERE `id` = $user_id";
    $result = mysqli_query($link, $query);
    $error = mysqli_error($link);

    //check for errors
    if (mysqli_error($link)) {
        error($error); //custom function
    }

    //get user, close connection and return user
    $user = mysqli_fetch_assoc($result);
    mysqli_close($link);
    return $user;
}

