var interval = null; //interval that updates the time value every second
var max = 100; //song duration
var music; //audio html tag and object
//this handles the loading and playing of the songs
//https://www.w3schools.com/HTML/html5_audio.asp
//https://www.w3schools.com/tags/ref_av_dom.asp
var path = '../assets/content/audio/songs/'; //absolute path to the audio files

var isPlaying = false; //whether a song is currently playing or not
var shuffle = false; //whether shuffle is on or off
var repeat = 0; //repeat setting
// 0 = no repeat
// 1 = repeat all songs
// 2 = repeat one songs

var songs = null; //list of songs to be played
//this is an array of the songs in your database, in the same format
//basically $song['artist_name'] is the same as banana.artist_name or banana[artist_name]
//https://www.w3schools.com/js/js_objects.asp
var originalSongs = null; //songs in it's original order
//this variable will never change
//use it to undo shuffle (I already gave you a hint!)

var currentSongIndex = 0; //index of the current song playing
//keep in mind songs is an array so like all array you access it by index'
//in this case it's a numeric array so index are 0,1,2...,n
var currentSong = null; //the current song being played
//basically songs[currentSongIndex]

$(document).ready(function () {
    music = document.getElementById('song');
    music.addEventListener('loadedmetadata', function () {
        max = parseInt(music.duration);
        var time = 0;
        try {
            $('#slider').slider('destroy');
            clearInterval(interval);
        } catch (err) {

        }
        $('#slider').slider({
            max: max
        });
        interval = window.setInterval(function () {
            console.log(time, max);
            $('#slider').slider('value', time++);
        }, 1000);
    });

    loadSongs();

    //init slider
    $('#slider').slider();

    music.addEventListener('timeupdate', function () {
        var duration = music.duration;
        var current = music.currentTime;
        $("#endTime").html(time(duration));
        $("#currentTime").html(time(current));
    });
});

function time(x) {
    var sec = new Number();
    var min = new Number();
    sec = Math.floor(x);
    min = Math.floor(sec / 60);
    min = min >= 10 ? min : '0' + min;
    sec = Math.floor(sec % 60);
    sec = sec >= 10 ? sec : '0' + sec;

    return min + ":" + sec;
}

function loadSongs() {
    $.ajax({
        url: 'http://localhost/spotify/api/json.php',
        method: 'get',
        success: function (response) {
            songs = JSON.parse(response);
            originalSongs = JSON.parse(response);
        },
        error: function (a, b, c) {
            alert(c.responseText);
        }
    })
}

/**
 * the button next to the song name
 * @param {type} btn
 * @param {type} song
 * @returns {undefined}
 */
function playSong(btn, song) {
    for (var x in songs) {
        var s = songs[x];
        if (s.file_path == song) {
            currentSongIndex = x;
        }
    }

    if (shuffle) {
        //swap
        var tmp = songs[0];
        songs[0] = songs[currentSongIndex];
        songs[currentSongIndex] = tmp;
        currentSongIndex = 0;
    }

    isPlaying = !$(btn).hasClass('pause');
    playToggle(document.getElementById('playPause'));
}

/**
 * Universal play/pause button
 * @param {type} btn
 * @returns {undefined}
 */
function playToggle() {
    if (isPlaying) {
        clearInterval(interval); //stops interval
        music.pause();
        changeIcon();
    } else {
        play();
    }
    isPlaying = !isPlaying;
}

function shuffleToggle() {
    var shuf;

    //changes icon
    if (shuffle) {
        shuf = '<i class="fa fa-random" aria-hidden="true" style="color: #808080"></i>';
    } else {
        shuf = '<i class="fa fa-random" aria-hidden="true"></i>';
    }
    document.getElementById('random').innerHTML = shuf;

    //toggles shuffle setting
    shuffle = !shuffle;

    if (shuffle) {
        songs = arrayShuffle(songs);
    } else {
        songs = originalSongs;
    }
}

function repeatToggle() {
    var iconRepeat;

    repeat++;
    if (repeat > 2) {
        repeat = 0;
    }

    /**
     * if no repeat - when you reach the end of your playlist, nothing plays again
     */
    if (repeat == 0) {
        iconRepeat = '<i class="fa fa-repeat" aria-hidden="true" style="color: #808080">';
    }
    /**
     * if repeat all - loop through all your songs
     */
    else if (repeat == 1) {
        iconRepeat = '<i class="fa fa-repeat" aria-hidden="true">';
    }
    /**
     * repeat one - loop the same song over and over
     */
    else if (repeat == 2) {
        iconRepeat = '<span class="relative"><i class="fa fa-repeat"><span class="add-on">1</span></i></span>';
    }

    document.getElementById('repeat').innerHTML = iconRepeat;
}

function flow() {
    if (repeat < 2) {
        //default
        currentSongIndex++;

        //current indx is 5, what now?
        if (currentSongIndex >= songs.length) {
            currentSongIndex = 0;
            if (repeat == 0) {
                load();
                return false;
            }
        }
    }

    play();
}

function load() {
    changeIcon();

    currentSong = songs[currentSongIndex];
    music.src = path + currentSong.file_path;
    music.load();
}

function play() {
    load();
    music.play();
    
    var btn = document.getElementById(currentSong.id);
    $(btn).removeClass('pause');
    $(btn).addClass('play');
    var btn = document.getElementById('playPause');
    $(btn).removeClass('pause');
    $(btn).addClass('play');
}

function changeIcon() {
    if (currentSong) {
        var btn = document.getElementById(currentSong.id);
        $(btn).removeClass('play');
        $(btn).addClass('pause');
        var btn = document.getElementById('playPause');
        $(btn).removeClass('play');
        $(btn).addClass('pause');
    }
}
/**
 * @link https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array#answer-6274381
 * Shuffles array in place.
 * @param {Array} a items The array containing the items.
 * @return {Array} the shuffled array
 */
function arrayShuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }

    return a;
}


function next() {
    currentSongIndex++;
    if (currentSongIndex > songs.length - 1) {
        currentSongIndex = 0;
    }
    play();
}

function prev() {
    currentSongIndex--;
    if (currentSongIndex < 0) {
        currentSongIndex = songs.length - 1;
    }
    play();
}