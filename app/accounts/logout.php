<?php

session_start();

$_SESSION['user_id'] = null;
unset($_SESSION['user_id']);
unset($_COOKIE['user_id']);
setcookie('user_id', -1, time() - 60 * 60);

session_destroy();

header("Location: ../index.php");
exit();

