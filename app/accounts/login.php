<?php
$skipCheck = true;

include_once '../../functions.php';
include_once '../../config.php';

$cond = isset($_POST['username']) && isset($_POST['password']);
if ($cond) {
    $conn = dbConnect(); //custom function
    $username = $_POST['username'];
    $password = md5($_POST['password']);

    $sql = "SELECT `id` FROM `users` WHERE `username`='$username' AND `password`='$password'";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        $user = mysqli_fetch_assoc($result);
        if (isset($_POST['remember']) && $_POST['remember'] = 'accepted') {
            //set cookie
            setcookie('user_id', $user['id'], time() + 60 * 60 * 24 * 30);
        } else {
            $_SESSION['user_id'] = $user['id'];
        }
        //redirect to index page
        $redirect = ROOT;
        header("Location: $redirect");
        exit();
    } else {
        errorMsg('Error: username or password is incorrect'); //custom function
    }
}
?>


<!DOCTYPE html>
<html>
    <head>
        <?php
        include_once '../../templates/head.php';
        ?>
    </head>
    <body>
        <div class="container">
            <div class="row" style="margin-top: 20px;">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="well">
                        <?php
                        //check for the save flag set after registration
                        if (isset($_GET['save']) && $_GET['save'] == 'success'):
                            ?>
                            <div class="alert alert-success">Thanks for signing up! You can now log in.</div>
                        <?php endif; ?>
                        <h1>Log in</h1>
                        <form action="" method="post" class="add-bottom">
                            <div class="form-group">
                                <label>Username:</label>
                                <input name="username" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Password:</label>
                                <input name="password" type="text" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-primary">log in</button>
                            &nbsp;&nbsp;
                            <input name="remember" type="checkbox" value="accepted"> Keep me logged in   
                        </form>
                        <div>
                            Don't have an account yet? <a href="register.php">Join today</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>

