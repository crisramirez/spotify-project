<?php

$skipCheck = true;

include_once '../../functions.php';
include_once '../../config.php';

//handle register
$cond = isset($_POST['username']) && isset($_POST['password']);

if ($cond) {
    //connect to db
    $conn = dbConnect(); //custom function

    //check for unique username
    $username = $_POST['username'];
    $sql = "SELECT * FROM `users` WHERE `username`='$username'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) != 0) {
        errorMsg('Error: username already exists'); //custom function
    } else {
        //save to db
        $pw = md5($_POST['password']);
        $sql = "INSERT INTO `users` (`username`, `password`, `name`) VALUES ('{$_POST['username']}', '{$pw}', '{$_POST['name']}')";
        if (mysqli_query($conn, $sql)) {
            //redirect to login page with save flag at the end to display the message
            $redirect = ROOT.'/accounts/login.php?save=success';
            header("Location: $redirect");
            exit();
        } else {
            errorMsg('Error: ' . mysqli_error($conn)); //custom function
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <?php
    include_once '../../templates/head.php';
    ?>
</head>
<body>

<div class="container">
    <div class="row" style="margin-top: 20px;">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="well">
                <h1>Join Today</h1>
                <form action="" method="post" class="add-bottom">
                    <div class="form-group">
                        <label> Name:</label>
                        <input name="name" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Username:</label>
                        <input name="username" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Password:</label>
                        <input name="password" type="password" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-primary">Create Account</button>
                </form>

                <div>
                    Already have an account? <a href="login.php">Log in</a>
                </div>
            </div>
        </div>
    </div>

</div>


</body>
</html>