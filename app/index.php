<!DOCTYPE html>
<?php
$conn = mysqli_connect('localhost', 'root', '', 'spotify');
mysqli_set_charset($conn, "utf8");
$sql = "SELECT * FROM songs";
$result = mysqli_query($conn, $sql);
?>


<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
        include_once '../templates/head.php';
        ?>

    </head>

    <?php
    include_once '../templates/header.php';
    ?>
    <body>
        <div class="jumbotron text-center">
            <h1>Music player</h1>
            <p>Search through your playlist and music</p> 
        </div>

        <div class="container">
            <?php if (mysqli_num_rows($result) > 0): ?>

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Song</th>
                            <th>Artist</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php while ($song = mysqli_fetch_assoc($result)): ?>
                            <tr>
                                <td>
                                    <button id="<?= $song['id'] ?>" type="button" class="btn btn-default pause" onclick="playSong(this, '<?= $song['file_path'] ?>')">
                                        <i class="fa fa-play"></i>
                                        <i class="fa fa-pause"></i>
                                    </button>
                                </td>
                                <td><?php echo $song['song_name'] ?></td>
                                <td><?php echo $song['song_artist'] ?></td>
                            </tr>
                        <?php endwhile; ?>
                    </tbody>
                </table>

                <audio id="song" onended="flow();"></audio>
                <div class="row add-bottom">
                    <div class="col-sm-2 text-right" id="currentTime"></div>
                    <div class="col-sm-8">
                        <div id="slider"></div>
                    </div>
                    <div class="col-sm-2 text-left" id="endTime"></div>
                </div>
                <div class="text-center">
                    <div class="btn-group btn-group-lg add-bottom">
                        <button type="button" class="btn btn-default" id="random" onclick="shuffleToggle()"><i class="fa fa-random" aria-hidden="true" style="color: #808080"></i></button>

                        <button type="button" class="btn btn-default" onclick="prev();"><i class="fa fa-step-backward" aria-hidden="true"></i></button>

                        <button type="button" class="btn btn-default pause" id="playPause" onclick="playToggle()">
                            <i class="fa fa-play"></i>
                            <i class="fa fa-pause"></i>
                        </button>

                        <button type="button" class="btn btn-default" onclick="next();"><i class="fa fa-step-forward" aria-hidden="true"></i></button>

                        <button type="button" class="btn btn-default" id="repeat" onclick="repeatToggle()"><i class="fa fa-repeat" aria-hidden="true" style="color: #808080"></i></button>
                    </div>
                </div>

            <?php else: ?>
                <h3>No songs found</h3>
            <?php endif; ?>
        </div>

    </body>
</html>
