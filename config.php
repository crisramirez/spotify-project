<?php

/**
 * This file will take care of all the common tasks all page loads will do:
 *
 * 1) defined constants that will be used across the whole app
 *      -this way you only change the value in one place if you need to make a change
 * 2) check if you are logged in and redirect to log in screen if applicable
 * 3) create the user array if logged in
 */

//define constants
defined('DB_HOST') or define('DB_HOST', 'localhost');
defined('DB_USERNAME') or define('DB_USERNAME', 'root');
defined('DB_PW') or define('DB_PW', '');
defined('DB_NAME') or define('DB_NAME', 'spotify');

defined('ROOT') or define('ROOT', 'http://localhost/spotify/app');
defined('CDN') or define('CDN', 'http://localhost/spotify/assets');

//check if logged in
session_start();

//if skip check is set and is true, skip log in check
//without this login.php would continue to redirect to itself killing the browser
//$skipCheck will only be set in login.php and register.php
$skipCheck = isset($skipCheck) ? $skipCheck : false;

if (!$skipCheck) {
    if (isset($_COOKIE['user_id'])) {
        $user_id = $_COOKIE['user_id'];
    } elseif (isset($_SESSION['user_id'])) {
        $user_id = $_SESSION['user_id'];
    } else {
        $login = ROOT . '/accounts/login.php';
        header("Location: $login");
        exit();
    }

    $user = getUser($user_id);
}
else {
    $user = null;
}

